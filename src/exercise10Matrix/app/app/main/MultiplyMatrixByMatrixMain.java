package exercise10Matrix.app.app.main;

import exercise10Matrix.app.app.app.MultiplyMatrixByMatrix;

/**
 * Created by Przemysław on 2017-03-18.
 */
public class MultiplyMatrixByMatrixMain {
    public static void main(String[] args) {
        MultiplyMatrixByMatrix e = new MultiplyMatrixByMatrix();
        int[][] arr = {{2, 1, 3}, {-1, 2, 4}};
        int[][] arr2 = {{1, 3}, {2, -2}, {-1, 4}};

        e.printArray(e.multiplyByMatrix(arr, arr2));
    }


}