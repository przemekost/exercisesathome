package exercise10Matrix.app.app.app;

/**
 * Created by Przemysław on 2017-03-18.
 */
public class MultiplyMatrixByMatrix {
    public int[][] multiplyByMatrix(int[][] arr, int[][] arr2) {
        int arrColumnLength = arr[0].length; // arr columns length
        int arr2RowLength = arr2.length;    // arr2 rows length
        if (arrColumnLength != arr2RowLength) return null; // matrix multiplication is not possible
        int finalRowLength = arr.length;    // arrFinal rows length
        int finalColLength = arr2[0].length; // arrFinal columns length
        int[][] finalArray = new int[finalRowLength][finalColLength];
        for (int i = 0; i < finalRowLength; i++) {         // rows from arr
            for (int j = 0; j < finalColLength; j++) {     // columns from arr2
                for (int k = 0; k < arrColumnLength; k++) { // columns from arr
                    finalArray[i][j] += arr[i][k] * arr2[k][j];
                }
            }
        }
        return finalArray;
    }

    public void printArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                System.out.print(arr[i][j]);
                System.out.print("   ");
            }
            System.out.println();
        }
    }
}