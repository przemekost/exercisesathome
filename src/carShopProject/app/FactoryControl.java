package carShopProject.app;


import carShopProject.utils.*;
import carShopProject.data.*;

/**
 * Created by Przemysław on 2017-03-18.
 */
public class FactoryControl {

    // variable to communicate with user
    private DataReader dataReader;

    // Factory store data
    private Factory factory;

    public FactoryControl() {
        dataReader = new DataReader();
        factory = new Factory();
    }

    public void loopForControlTheProgram() {
        Option option;
        printOptions();
        while ((option = Option.createFromInt(dataReader.getInt())) != Option.EXIT) {
            switch (option) {
                case ADD_CAR:
                    addCar();
                    break;
                case ADD_MOTORCYCLE:
                    addMotorcycle();
                    break;
                case PRINT_CARS:
                    printCars();
                    break;
                case PRINT_MOTORCYCLES:
                    printMotorcycles();
                    break;

            }
            printOptions();
        }
        dataReader.close();
    }

    private void printOptions() {
        System.out.println("Select an option: ");
        for(Option o : Option.values()){
            System.out.println(o);
        }
    }

    private void addCar() {
        Car car = dataReader.readAndCreateCar();
        factory.addCar(car);
    }

    private void printCars() {
        factory.printCars();
    }
    private void addMotorcycle() {
        Motorcycle motorcycle = dataReader.readAndCreateMotorcycle();
        factory.addMotorcycles(motorcycle);
    }

    private void printMotorcycles() {
        factory.printMotorcycles();
    }


}
