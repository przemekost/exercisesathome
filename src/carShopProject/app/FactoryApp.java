package carShopProject.app;

/**
 * Created by Przemysław on 2017-03-18.
 */
public class FactoryApp {
    public static void main(String[] args) {
        final String appName = "CarAndMotorcycleFactory v0.2";
        System.out.println("Application name: " + appName);

        FactoryControl factoryControl = new FactoryControl();
        factoryControl.loopForControlTheProgram();

    }
}
