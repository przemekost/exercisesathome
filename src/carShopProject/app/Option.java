package carShopProject.app;

/**
 * Created by Przemysław on 2017-03-19.
 */
public enum Option {
    EXIT(0, "Exit"),
    ADD_CAR(1, "Add a car"),
    ADD_MOTORCYCLE(2, "Add a motorcycle"),
    PRINT_CARS(3, "Print cars"),
    PRINT_MOTORCYCLES(4, "Print motorcycles");

    private int value;
    private String description;

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    //type option store two variables: value - option which can be select by user and description for this option
    Option(int value, String description){
        this.value = value;
        this.description=description;
    }
    //override method in other to print information about all options
    @Override
    public String toString() {
        return value + " - "+description;
    }

    //for create Option value from int, method values() <-- can print array with all values
    public static Option createFromInt(int option){
        return Option.values()[option];
    }
}
