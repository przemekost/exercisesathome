package carShopProject.data;

import java.util.List;

/**
 * Created by Przemysław on 2017-03-18.
 */
public class Car extends Vehicle {
    public Car(String brand, String model, String body, int productionYear, String color) {
        super(brand, model, productionYear, color);
        this.body = body;
    }

    private String body;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Car car = (Car) o;

        return body != null ? body.equals(car.body) : car.body == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (body != null ? body.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return getBrand() + "; " + getModel() + "; " + getBody() + "; " + getProductionYear()
                + "; " + getColor();
    }
}