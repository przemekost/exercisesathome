package carShopProject.data;

/**
 * Created by Przemysław on 2017-03-18.
 */
public class Motorcycle extends Vehicle {
    private String motorcycleClass;

    public String getMotorcycleClass() {
        return motorcycleClass;
    }

    public void setMotorcycleClass(String motorcycleClass) {
        this.motorcycleClass = motorcycleClass;
    }

    public Motorcycle(String brand, String model, String motorcycleClass, int productionYear, String color) {
        super(brand, model, productionYear, color);
        this.motorcycleClass = motorcycleClass;
    }

    @Override
    public String toString() {
        return getBrand() + "; " + getModel() + "; " + getMotorcycleClass() + "; " + getProductionYear()
                + "; " + getColor();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Motorcycle that = (Motorcycle) o;

        return motorcycleClass != null ? motorcycleClass.equals(that.motorcycleClass) : that.motorcycleClass == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (motorcycleClass != null ? motorcycleClass.hashCode() : 0);
        return result;
    }
}
