package carShopProject.data;

/**
 * Created by Przemysław on 2017-03-18.
 */
public class Factory {
    public final static int MAX_VEHICLES = 2000;
    private Vehicle[] vehicles;
    private int vehiclesNumbers;

    public Vehicle[] getVehicles() {
        return vehicles;
    }

    public int getVehiclesNumbers() {
        return vehiclesNumbers;
    }

    public void setVehicles(Vehicle[] vehicles) {
        this.vehicles = vehicles;
    }

    public Factory() {
        setVehicles(new Vehicle[MAX_VEHICLES]);
    }

    public void addVehicle(Vehicle veh) {
        if (vehiclesNumbers < MAX_VEHICLES) {
            vehicles[vehiclesNumbers] = veh;
            vehiclesNumbers++;
        } else {
            System.out.println("Max number of vehicles is on store");
        }
    }

    public void addCar(Car car) {
        addVehicle(car);
    }

    public void printCars() {
        int countCars = 0;
        for (int i = 0; i < vehiclesNumbers; i++) {
            if (vehicles[i] instanceof Car) {
                System.out.println(vehicles[i]);
                countCars++;
            }
        }

        if (countCars == 0) {
            System.out.println("We don't have any car on store");
        }
    }

    public void addMotorcycles(Motorcycle motorcycle) {
        addVehicle(motorcycle);
    }

    public void printMotorcycles() {
        int countMotorcycles = 0;
        for (int i = 0; i < vehiclesNumbers; i++) {
            if (vehicles[i] instanceof Motorcycle) {
                System.out.println(vehicles[i]);
                countMotorcycles++;
            }
        }

        if (countMotorcycles == 0) {
            System.out.println("We don't have any motorcycles on store");
        }
    }

}