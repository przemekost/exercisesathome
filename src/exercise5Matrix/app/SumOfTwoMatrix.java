package exercise5Matrix.app;

/**
 * Created by Przemysław on 2017-03-18.
 */
public class SumOfTwoMatrix {

    public int[][] sumOfTwoMatrix(int[][] arr, int[][] arr2) {
        int sum = 0;
        int[][] result = new int[arr.length][arr[0].length];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                result[i][j] = arr[i][j] + arr2[i][j];
            }
        }
        return result;
    }

    public void printArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                System.out.printf("%7d", arr[i][j]);
            }
            System.out.println();
        }

    }
}
