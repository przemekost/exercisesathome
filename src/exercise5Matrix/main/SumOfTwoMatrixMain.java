package exercise5Matrix.main;

import exercise5Matrix.app.SumOfTwoMatrix;

/**
 * Created by Przemysław on 2017-03-18.
 */
public class SumOfTwoMatrixMain {
    public static void main(String[] args) {
        int[][] array1 = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
        int[][] array2 = { { 12,5,6 }, { 24, 45, 6 }, { 67, 81, 19 } };


        SumOfTwoMatrix e = new SumOfTwoMatrix();
        e.printArray(e.sumOfTwoMatrix(array1,array2));
    }

}
