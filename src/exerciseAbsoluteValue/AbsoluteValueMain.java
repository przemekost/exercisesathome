package exerciseAbsoluteValue;

import java.util.Scanner;

/**
 * Created by Przemysław on 2017-03-18.
 */
public class AbsoluteValueMain {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Type number in order to calculate absolute value");
        int a = sc.nextInt();

        AbsoluteValue e = new AbsoluteValue();

        System.out.println("Absolute value from number: " + a + " is: " + e.absoluteValue(a));
    }
}
