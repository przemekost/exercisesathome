package exerciseAbsoluteValue;

/**
 * Created by Przemysław on 2017-03-18.
 */
public class AbsoluteValue {

    public int absoluteValue(int a){
        int absoluteValue;

        if(a>0){
            absoluteValue=a;

        }else if(a==0){
            absoluteValue=0;
        }else{
            absoluteValue=a*(-1);
        }
        return absoluteValue;
    }

}
